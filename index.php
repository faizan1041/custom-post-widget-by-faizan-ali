<?php
/*
Plugin Name: Custom Post Widget by Faizan Ali
Plugin URI: http://faizan-ali.com/
Description: A widget which displays custom post type where ever you want.
Author: Faizan Ali
Version: 1.0
Author URI: http://faizan-ali.com/
*/










// register Custom_Post_Widget widget
function register_custom_post_widget() {
    register_widget( 'Custom_Post_Widget' );
}
add_action( 'widgets_init', 'register_custom_post_widget' );
















/**
 * Adds Custom_Post_Widget widget.
 */
class Custom_Post_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'custom_post_widget', // Base ID
			__('Custom Post Widget', 'text_domain'), // Name
			array( 'description' => __( 'A Custom Post Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

     	        echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		$post_type = $instance['post_type'];



		$args = array(
                'post_type' => $post_type,
                'posts_per_page' =>  $instance['number_of_posts'],

            );






        $qry = new WP_Query($args);


		if($qry->have_posts()){
				while ( $qry->have_posts() ) {

				 $qry->the_post();

				 $custom_permalink = get_permalink();


				  echo '<div><h5><a href="'. $custom_permalink .'">'. get_the_title() . '</a></h5>';
				   echo '<a href="'. $custom_permalink .'">'. get_the_post_thumbnail() . '</a><br/>';
					echo get_the_excerpt( ) . '</div>';

			 }

		}


		wp_reset_postdata();


		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {


		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php







		if ( isset( $instance[ 'post_type' ] ) ) {
			$post_type = $instance[ 'post_type' ];
		}
		else {
			$post_type = __( 'post', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post Type:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" type="text" value="<?php echo esc_attr( $post_type ); ?>">
		</p>
		<?php


		if ( isset( $instance[ 'number_of_posts' ] ) ) {
			$number_of_posts = $instance[ 'number_of_posts' ];
		}
		else {
			$number_of_posts = __( '-1', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'number_of_posts' ); ?>"><?php _e( 'Number of Posts:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_posts' ); ?>" name="<?php echo $this->get_field_name( 'number_of_posts' ); ?>" type="number" value="<?php echo esc_attr( $number_of_posts ); ?>">
		</p>
		<?php













	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		$instance['post_type'] = ( ! empty( $new_instance['post_type'] ) ) ? strip_tags( $new_instance['post_type'] ) : '';

		$instance['number_of_posts'] = ( ! empty( $new_instance['number_of_posts'] ) ) ? strip_tags( $new_instance['number_of_posts'] ) : '';






		return $instance;
	}

} // class Custom_Post_Widget
